module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'airbnb',
    'airbnb/hooks',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'eslint-plugin-html',
    'lodash-fp',
    'react',
  ],
  rules: {
    'comma-dangle': [1, 'always-multiline'],
    'no-underscore-dangle': 0,
    'max-len': [1, 180, 4],
    'arrow-body-style': [0],
    'react/jsx-no-bind': [0],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'lodash-fp/consistent-compose': 'off',
    'lodash-fp/consistent-name': [
      'error',
      '_',
    ],
    'lodash-fp/no-argumentless-calls': 'error',
    'lodash-fp/no-chain': 'error',
    'lodash-fp/no-extraneous-args': 'error',
    'lodash-fp/no-extraneous-function-wrapping': 'error',
    'lodash-fp/no-extraneous-iteratee-args': 'error',
    'lodash-fp/no-extraneous-partials': 'error',
    'lodash-fp/no-for-each': 'off',
    'lodash-fp/no-partial-of-curried': 'error',
    'lodash-fp/no-single-composition': 'error',
    'lodash-fp/no-submodule-destructuring': 'error',
    'lodash-fp/no-unused-result': 'error',
    'lodash-fp/prefer-compact': 'error',
    'lodash-fp/prefer-composition-grouping': 'error',
    'lodash-fp/prefer-constant': [
      'error',
      {
        arrowFunctions: false,
      },
    ],
    'lodash-fp/prefer-flat-map': 'error',
    'lodash-fp/prefer-get': 'error',
    'lodash-fp/prefer-identity': [
      'error',
      {
        arrowFunctions: false,
      },
    ],
    'lodash-fp/preferred-alias': 'off',
    'lodash-fp/use-fp': 'error',
  },
};
