import React from 'react';
import _ from 'lodash/fp';
import { Route, Switch, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Home from './components/home';

const styles = {};

const App = () => {
  return (
    <Route>
      <Switch>
        <Route path="/" component={Home} />
      </Switch>
    </Route>
  );
};

export default _.compose(
  withRouter,
  withStyles(styles),
)(App);
