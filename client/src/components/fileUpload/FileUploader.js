/* eslint no-console: 0 */ // --> OFF
/* eslint react/jsx-props-no-spreading: 0 */
import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';

const FileUploader = (props) => {
  const { onFileReadComplete } = props;

  const csvToArray = (str, delimiter = ',') => {
    // slice from start of text to the first \n index
    // use split to create an array from string by delimiter
    // lastly, remove all line breaks from value
    const headers = str.slice(0, str.indexOf('\n')).split(delimiter).map((header) => header.replace(/(\r\n|\n|\r)/gm, ''));
    // console.log('headers: ', headers);

    // slice from \n index + 1 to the end of the text
    // use split to create an array of each csv value row
    // lastly, remove all line breaks from value
    const rows = str.slice(str.indexOf('\n') + 1).split('\n').map((row) => row.replace(/(\r\n|\n|\r)/gm, ''));
    // console.log('rows: ', rows);

    // Map the rows
    // split values from each row into an array
    // use headers.reduce to create an object
    // object properties derived from headers:values
    // the object passed as an element of the array
    const arr = rows.map((row) => {
      const values = row.split(delimiter);
      const el = headers.reduce((object, header, index) => {
        // eslint-disable-next-line no-param-reassign
        object[header] = values[index];
        return object;
      }, {});
      return el;
    });

    // return the array
    return arr;
  };

  const onFileDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      const reader = new FileReader();

      reader.onabort = () => console.log('file reading was aborted');
      reader.onerror = () => console.log('file reading has failed');
      reader.onload = () => {
        // Do whatever you want with the file contents
        const text = reader.result;
        const data = csvToArray(text);
        onFileReadComplete(data);
      };
      reader.readAsText(file);
    });
  }, [onFileReadComplete]);

  const {
    acceptedFiles,
    fileRejections,
    getRootProps,
    getInputProps,
  } = useDropzone({
    accept: 'text/plain, text/csv',
    onDrop: onFileDrop,
  });

  const acceptedFileItems = acceptedFiles.map((file) => (
    <li key={file.path}>
      {file.path}
      {' '}
      -
      {' '}
      {file.size}
      {' '}
      bytes
    </li>
  ));

  const fileRejectionItems = fileRejections.map(({ file, errors }) => (
    <li key={file.path}>
      {file.path}
      {' '}
      -
      {' '}
      {file.size}
      {' '}
      bytes
      <ul>
        {errors.map((e) => (
          <li key={e.code}>{e.message}</li>
        ))}
      </ul>
    </li>
  ));

  return (
    <section>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <p>Drag &rsquo;n&rsquo; drop some files here, or click to select files</p>
        <em>(Only *.jpeg and *.png images will be accepted)</em>
      </div>
      <aside>
        <h4>Accepted files</h4>
        <ul>{acceptedFileItems}</ul>
        <h4>Rejected files</h4>
        <ul>{fileRejectionItems}</ul>
      </aside>
    </section>
  );
};

FileUploader.defaultProps = {
  onFileReadComplete: PropTypes.func.isRequired,
};

FileUploader.propTypes = {
  onFileReadComplete: PropTypes.func,
};

export default FileUploader;
