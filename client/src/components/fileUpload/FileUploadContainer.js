/* eslint no-console: 0 */ // --> OFF
/* eslint react/jsx-props-no-spreading: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash/fp';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import {
  Grid,
  Paper,
  withStyles,
} from '@material-ui/core';
import classNames from 'classnames';
import FileUploader from './FileUploader';

const getColor = (props) => {
  if (props.isDragAccept) {
    return '#00e676';
  }
  if (props.isDragReject) {
    return '#ff1744';
  }
  if (props.isDragActive) {
    return '#2196f3';
  }
  return '#eeeeee';
};

const StyledWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${(props) => getColor(props)};
  border-style: dashed;
  color: #bdbdbd;
  outline: none;
  transition: border .24s ease-in-out;
`;

const styles = (theme) => ({
  customerIDList: {

  },
  paper: {
    padding: theme.spacing(1),
  },
});

class FileUploadContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customerIDs: [],
    };
  }

  componentDidMount() { }

  onFileReadComplete = (customerIDArr) => {
    console.log('customerIDArr: ', customerIDArr);
    if (customerIDArr.length) {
      this.setState({
        customerIDs: customerIDArr,
      });
    }
  }

  renderCustomerIDs = (customerIDs) => {
    return customerIDs.map((customer) => <li key={customer.customerID}>{customer.customerID}</li>);
  }

  render() {
    const { classes } = this.props;
    const { customerIDs } = this.state;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={4} />
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <StyledWrapper>
                <FileUploader onFileReadComplete={this.onFileReadComplete} />
              </StyledWrapper>
            </Paper>
          </Grid>
          <Grid item xs={4} />
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={4} />
          <Grid item xs={4}>
            {customerIDs.length ? <Paper className={classNames(classes.customerIDList, classes.paper)}>{this.renderCustomerIDs(customerIDs)}</Paper> : ''}
          </Grid>
          <Grid item xs={4} />
        </Grid>
      </div>
    );
  }
}

FileUploadContainer.defaultProps = {
  classes: PropTypes.shape({}).isRequired,
};

FileUploadContainer.propTypes = {
  classes: PropTypes.shape({
    customerIDList: PropTypes.string,
    paper: PropTypes.string,
  }),
};

export default _.compose(
  withRouter,
  withStyles(styles),
)(FileUploadContainer);
