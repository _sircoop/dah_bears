/* eslint no-console: 0 */ // --> OFF
import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Grid,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import FileUploadContainer from '../fileUpload';
// import CONSTANTS from '../../constants';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const Home = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h3" gutterBottom align="center">
            Chicago Bears
          </Typography>
        </Grid>
      </Grid>
      <FileUploadContainer />
    </div>
  );
};

Home.defaultProps = {
};

Home.propTypes = {
};

export default withRouter(Home);
