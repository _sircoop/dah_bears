function getAPIROOT() {
  if (process.env.NODE_ENV === 'staging') {
    return process.env.STAGING_API_ROOT;
  } if (process.env.NODE_ENV === 'production') {
    return process.env.PROD_API_ROOT;
  }
  return process.env.DEV_API_ROOT;
}

const CONSTANTS = {
  API_ROOT: getAPIROOT(),
};

export default CONSTANTS;
